from django.db import models

class dbTestModel(models.Model):
    id = models.CharField(max_length=10, primary_key=True)
    rand = models.CharField(max_length=3)
